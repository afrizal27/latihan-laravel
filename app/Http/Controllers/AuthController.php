<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function form(){
        return view('halaman.form');
    }

    public function kirim(Request $request){
        $firstName = $request['first_name'];
        $lastName = $request['last_name'];
        return view('halaman.welcome', compact('firstName','lastName'));
    }
}
